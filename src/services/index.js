const axios = require("axios");

export const getActivity = async payload => {
  const result = await axios.get("https://www.boredapi.com/api/activity/", {
    params: payload
  });
  return result.data;
};
