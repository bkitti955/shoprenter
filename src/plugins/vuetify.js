import Vue from "vue";
import Vuetify from "vuetify/lib";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#5CBABC",
        secondary: "#AEDDDE",
        accent: "#E0F2F1"
      }
    }
  }
});
