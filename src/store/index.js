import Vue from "vue";
import Vuex from "vuex";
import VuexPersistence from "vuex-persist";

import { getActivity } from "../services";

Vue.use(Vuex);

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
});

export default new Vuex.Store({
  state: {
    activity: {},
    activities: []
  },
  mutations: {
    setActivity(state, activity) {
      state.activity = activity;
    },
    saveActivity(state) {
      const alreadyExist = state.activities.some(
        activity => state.activity.key === activity.key
      );
      if (alreadyExist) return;
      state.activities = [state.activity, ...state.activities];
    },
    clearActivities(state) {
      state.activities = [];
    },
    removeActivity(state, key){
      state.activities = state.activities.filter((activity) => activity.key !== key);
    }
  },
  actions: {
    async getActivity({ commit }, payload) {
      const activity = await getActivity(payload);
      commit("setActivity", activity);
    },
    saveActivity({ commit }) {
      commit("saveActivity");
    },
    clearActivities({ commit }) {
      commit("clearActivities");
    },
    removeActivity({ commit }, key) {
      commit("removeActivity", key);
    }
  },
  getters: {
    activitiesWithId(state){
      return state.activities.map((activity, id) => ({...activity, id: id+1}))
    },
    alreadyHasActivity(state) {
      return state.activities.some((activity)=>state.activity.key === activity.key);
    }
  },
  plugins: [vuexLocal.plugin]
});
