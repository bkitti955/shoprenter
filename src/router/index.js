import Vue from "vue";
import VueRouter from "vue-router";
import Activity from "../views/Activity.vue";
import List from "../views/List.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Activity",
    component: Activity
  },
  {
    path: "/list",
    name: "List",
    component: List
  }
];

const router = new VueRouter({
  routes
});

export default router;
